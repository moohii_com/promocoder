<?php
/**
 * Seve Orders attached to promocode.
 */
class msPromocoderOrdersCreateProcessor extends modObjectCreateProcessor {
  public $objectType = 'msPromocoderOrders';
  public $classKey = 'msPromocoderOrders';
  public $primaryKeyField = 'code_id';

  /**
   * Process the Object create processor
   * {@inheritDoc}
   * @return mixed
   */
  public function process() {
    /* Run the beforeSet method before setting the fields, and allow stoppage */
    $canSave = $this->beforeSet();
    if ($canSave !== true) {
      return $this->failure($canSave);
    }
    $this->object->fromArray($this->getProperties(), '', true);
    /* run the before save logic */
    $canSave = $this->beforeSave();
    if ($canSave !== true) {
      return $this->failure($canSave);
    }
    /* run object validation */
    if (!$this->object->validate()) {
      /** @var modValidator $validator */
      $validator = $this->object->getValidator();
      if ($validator->hasMessages()) {
        foreach ($validator->getMessages() as $message) {
          $this->addFieldError($message['field'],$this->modx->lexicon($message['message']));
        }
      }
    }

    $preventSave = $this->fireBeforeSaveEvent();
    if (!empty($preventSave)) {
      return $this->failure($preventSave);
    }

    /* save element */
    if ($this->object->save() == false) {
      $this->modx->error->checkValidation($this->object);
      return $this->failure($this->modx->lexicon($this->objectType.'_err_save'));
    }

    $this->afterSave();

    $this->fireAfterSaveEvent();
    $this->logManagerAction();
    return $this->cleanup();
  }

}

return 'msPromocoderOrdersCreateProcessor';