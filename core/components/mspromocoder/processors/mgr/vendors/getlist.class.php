<?php

/**
 * Get a list of Vendors.
 */
class msPromocoderVendorsGetListProcessor extends modObjectGetListProcessor {
  public $objectType = 'msVendor';
  public $classKey = 'msVendor';


  /**
   * Reininitialize process()
   * @return mixed
   */
  public function process() {
    $beforeQuery = $this->beforeQuery();
    if ($beforeQuery !== true) {
      return $this->failure($beforeQuery);
    }
    $data = $this->getData();
    $list = $this->iterate($data);
    foreach ($list as $k => $v) {
      $list[$k] = array(
        'id' => $v['id'],
        'value' => "[{$v['id']}] " . $v['name']
      );
    }

    return $this->outputArray($list,$data['total']);
  }

  /**
   * Reininitialize prepareQueryBeforeCount()
   *
   * @param xPDOQuery $c
   *
   * @return xPDOQuery
   */
  public function prepareQueryBeforeCount(xPDOQuery $c) {
    $query = trim($this->getProperty('query'));

    if ($this->getProperty('valuesqry') == true) {
      $query = explode('|', $query);
      $c->where(array(
        'id:IN' => $query
      ));
    }
    else {
      $search = explode(' ', $query);
      if (!empty($search)) {
        $search_array = array();
        foreach ($search as $w) {
          $search_array[] = array('name:LIKE' => '%' . $w . '%');
        }

        $c->where($search_array, xPDOQuery::SQL_OR);
      }
    }

    return $c;
  }

}

return 'msPromocoderVendorsGetListProcessor';