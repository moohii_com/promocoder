<?php

/**
 * Get a list of Products.
 */
class msPromocoderProductsGetListProcessor extends modObjectGetListProcessor {
  public $objectType = 'msPromocoderProducts';
  public $classKey = 'msPromocoderProducts';

  /**
   * {@inheritDoc}
   * @return mixed
   */
  public function process() {
    $beforeQuery = $this->beforeQuery();
    if ($beforeQuery !== true) {
      return $this->failure($beforeQuery);
    }

    return $this->getProducts();
  }

  /**
   * Get the data of the query.
   *
   * @return string (json)
   */
  public function getProducts() {
    $msPromocoder = $this->modx->getService('msPromocoder');
    $query = trim($this->getProperty('query'));

    if ($this->getProperty('valuesqry') == true) {
      $query = explode('|', $query);

      $q = $this->modx->newQuery('modResource');
      $q->where(array(
        'id:IN' => $query,
        'class_key' => 'msProduct'
      ));
      $q->limit(0);
      $q->select('id,pagetitle,pagetitle as value');
      $array = array();
      if ($q->prepare() && $q->stmt->execute()) {
        $array = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
      }

      foreach ($array as $k => $prod) {
        $array[$k]['pagetitle'] = "{$prod['pagetitle']} [{$prod['id']}]";
      }

      return $this->outputArray($array, count($array));
    }
    elseif ($msPromocoder->mSearch2) {
      // Run msearch2 processor to get products.
      $response = $this->modx->runProcessor(
        'mgr/search/getlist',
        array(
          'query' => $query
        ),
        array(
          'processors_path' => MODX_CORE_PATH . 'components/msearch2/processors/'
      ));
    }
    else {
      // Run minishop2 processor to get products.
      $response = $this->modx->runProcessor(
        'mgr/product/getlist',
        array(
          'query' => $query
        ),
        array(
          'processors_path' => MODX_CORE_PATH . 'components/minishop2/processors/'
      ));
    }

    $response = $this->modx->fromJSON($response->response);
    $response = (array)$response;

    foreach ($response['results'] as $k => $p) {
      if ($p['class_key'] != 'msProduct') {
        unset($response['results'][$k]);
        continue;
      }

      $product['id'] = $p['id'];
      $product['value'] = $p['pagetitle'] . " [{$p['id']}]";
      $product['pagetitle'] = $p['pagetitle'] . " [{$p['id']}]";

      $response['results'][$k] = $product;
      unset($product);
    }

    return $this->modx->toJSON($response);
  }

}

return 'msPromocoderProductsGetListProcessor';