<?php

/**
 * Get an Code
 */
class msPromocoderProductsGetProcessor extends modObjectGetProcessor {
  public $objectType = 'msPromocoderProducts';
  public $classKey = 'msPromocoderProducts';
  public $primaryKeyField = 'code_id';

}

return 'msPromocoderProductsGetProcessor';