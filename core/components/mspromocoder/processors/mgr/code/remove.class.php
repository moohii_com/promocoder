<?php

/**
 * Remove an Codes
 */
class msPromocoderCodesRemoveProcessor extends modObjectProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';
	public $classKeyProducts = 'msPromocoderProducts';
	public $classKeyCategoriesVendors = 'msPromocoderCategoriesVendors';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('mspromocoder_code_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var msPromocoderCodes $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('mspromocoder_code_err_nf'));
			}

			$type = $object->get('type');

			$object->remove();

			switch ($type) {
				case 1:
					// Remove products attached to promocode.
					if ($this->modx->getObject($this->classKeyProducts, array('code_id' => $id))) {
						$result = $this->modx->query("DELETE FROM {$this->modx->getTableName($this->classKeyProducts)} WHERE code_id=$id");
					}
					break;
				case 2:
					// Remove categories/vendors attached to promocode.
					if ($this->modx->getObject($this->classKeyCategoriesVendors, array('code_id' => $id))) {
						$result = $this->modx->query("DELETE FROM {$this->modx->getTableName($this->classKeyCategoriesVendors)} WHERE code_id=$id");
					}
					break;
			}
		}

		return $this->success();
	}

}

return 'msPromocoderCodesRemoveProcessor';