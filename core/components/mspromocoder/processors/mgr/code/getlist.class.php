<?php

/**
 * Get a list of Codes
 */
class msPromocoderCodesGetListProcessor extends modObjectGetListProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';
	public $defaultSortField = 'id';
	public $defaultSortDirection = 'DESC';
	//public $permission = 'list';


	/**
	 * * We doing special check of permission
	 * because of our objects is not an instances of modAccessibleObject
	 *
	 * @return boolean|string
	 */
	public function beforeQuery() {
		if (!$this->checkPermissions()) {
			return $this->modx->lexicon('access_denied');
		}

		return true;
	}


	/**
	 * @param xPDOQuery $c
	 *
	 * @return xPDOQuery
	 */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		$query = trim($this->getProperty('query'));
		if ($query) {
			$c->where(array(
				'code:LIKE' => "%{$query}%",
				'OR:description:LIKE' => "%{$query}%",
			));
		}
		$type = trim($this->getProperty('type'));
		if ($type != '') {
			$c->where(array(
				'type' => (int)$type
			));
		}

		return $c;
	}


	/**
	 * @param xPDOObject $object
	 *
	 * @return array
	 */
	public function prepareRow(xPDOObject $object) {
		$array = $object->toArray();
		$discount =& $array['discount'];
		if (strpos($discount, '%') === false) {
			$discount = $discount . ' грн.';
		}
		$array['actions'] = array();

		// Edit
		$array['actions'][] = array(
			'cls' => '',
			'icon' => 'icon icon-edit',
			'title' => $this->modx->lexicon('mspromocoder_code_update'),
			//'multiple' => $this->modx->lexicon('mspromocoder_codes_update'),
			'action' => 'updateCode',
			'button' => true,
			'menu' => true,
		);

		if (!$array['active']) {
			$array['actions'][] = array(
				'cls' => '',
				'icon' => 'icon icon-power-off action-green',
				'title' => $this->modx->lexicon('mspromocoder_code_enable'),
				'multiple' => $this->modx->lexicon('mspromocoder_codes_enable'),
				'action' => 'enableCode',
				'button' => true,
				'menu' => true,
			);
		}
		else {
			$array['actions'][] = array(
				'cls' => '',
				'icon' => 'icon icon-power-off action-gray',
				'title' => $this->modx->lexicon('mspromocoder_code_disable'),
				'multiple' => $this->modx->lexicon('mspromocoder_codes_disable'),
				'action' => 'disableCode',
				'button' => true,
				'menu' => true,
			);
		}

		// Remove
		$array['actions'][] = array(
			'cls' => '',
			'icon' => 'icon icon-trash-o action-red',
			'title' => $this->modx->lexicon('mspromocoder_code_remove'),
			'multiple' => $this->modx->lexicon('mspromocoder_codes_remove'),
			'action' => 'removeCode',
			'button' => true,
			'menu' => true,
		);

		return $array;
	}

}

return 'msPromocoderCodesGetListProcessor';