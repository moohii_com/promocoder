<?php

/**
 * Get a list of Codes
 */
class msPromocoderCodeTypesGetListProcessor extends modObjectGetListProcessor {
  public $objectType = 'msPromocoderCodeTypes';
  public $classKey = 'msPromocoderCodeTypes';
  public $defaultSortField = 'id';


  /**
   * * We doing special check of permission
   * because of our objects is not an instances of modAccessibleObject
   *
   * @return boolean|string
   */
  public function beforeQuery() {
    if (!$this->checkPermissions()) {
      return $this->modx->lexicon('access_denied');
    }

    return true;
  }


  /**
   * @param xPDOObject $object
   *
   * @return array
   */
  public function prepareRow(xPDOObject $object) {
    $array = $object->toArray();

    return $array;
  }

}

return 'msPromocoderCodeTypesGetListProcessor';