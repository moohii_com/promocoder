<?php

/**
 * Enable an Code
 */
class msPromocoderCodesEnableProcessor extends modObjectProcessor {
	public $objectType = 'msPromocoderCodes';
	public $classKey = 'msPromocoderCodes';
	// public $languageTopics = array('mspromocoder');
	//public $permission = 'save';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('mspromocoder_code_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var msPromocoderCodes $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('mspromocoder_code_err_nf'));
			}

			$object->set('active', true);
			$object->save();
		}

		return $this->success();
	}

}

return 'msPromocoderCodesEnableProcessor';
