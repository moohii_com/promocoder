<?php
$xpdo_meta_map['msPromocoderCategoriesVendors']= array (
  'package' => 'mspromocoder',
  'version' => '1.1',
  'table' => 'ms_promocoder_categories_vendors',
  'extends' => 'xPDOObject',
  'fields' => 
  array (
    'code_id' => NULL,
    'categories' => NULL,
    'vendors' => NULL,
  ),
  'fieldMeta' => 
  array (
    'code_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'int',
      'null' => true,
    ),
    'categories' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => true,
    ),
    'vendors' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => true,
    ),
  ),
  'indexes' => 
  array (
    'PRIMARY' => 
    array (
      'alias' => 'PRIMARY',
      'primary' => true,
      'unique' => true,
      'columns' => 
      array (
        'code_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'msPromocoderCategoriesVendors' => 
    array (
      'class' => 'msPromocoderCodes',
      'local' => 'code_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
