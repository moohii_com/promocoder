<?php
/**
 * @plugin msPromocoder
 */

 $msPromocoder = $modx->getService(
    'msPromocoder',
    'msPromocoder',
    MODX_CORE_PATH . 'components/mspromocoder/model/mspromocoder/'
);

/** @var msDiscount $msDiscount */
$msDiscount = $modx->getService('msDiscount');

if (!$msPromocoder) {
    @session_write_close();
    exit('Could not initialize msPromocoder');
}

switch ($modx->event->name) {
  case 'OnHandleRequest':
  case 'OnLoadWebDocument':
    $isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

    if (empty($_REQUEST['ms_promocoder_action']) || ($isAjax && $modx->event->name != 'OnHandleRequest') || (!$isAjax && $modx->event->name != 'OnLoadWebDocument')) {
        return;
    }

    $action = trim($_REQUEST['ms_promocoder_action']);
    $only_cart = isset($_REQUEST['only_cart']) ? isset($_REQUEST['only_cart']) : false;

    switch ($action) {
    case 'cart/with_discont':
        $response = $msPromocoder->get_updated_cart($only_cart);
        break;

    case 'cart/without_discont':
        $response = $msPromocoder->get_deault_cart();
        break;

    case 'check/code':
        $code = trim($_REQUEST['promocode']);

        if ($msDiscount) {
            $response = $msDiscount->checkCoupon($code);
            if ($response === true) {
                exit('coupon');
            }
        }

        exit('false');
        break;

      default:
        $response = $msPromocoder->error('Unknown action!');
        break;
    }

    if ($isAjax) {
      @session_write_close();
      exit($response);
    }
    break;

    case 'msOnGetOrderCost':
        /**@var msOrderInterface $order */
        if (!empty($with_cart) && !empty($cost)) {
            if ($data = $order->get()) {
                if (!empty($data['mspromocoder_code'])) {
                    if ($order_price = $msPromocoder->get_discounted_cost()) {
                        $modx->event->returnedValues['cost'] = $order_price;
                    }
                }
                elseif ($msDiscount) {
                    if (!empty($data['coupon_code']) && $msDiscount->checkCoupon($data['coupon_code']) === true) {
                        if ($discount = $msDiscount->getCouponDiscount($data['coupon_code'], $cost)) {
                            $cost -= $discount;
                            if ($cost >= 0) {
                                $modx->event->returnedValues['cost'] = $cost;
                            }
                        }
                    }
                }
            }
        }
        break;

    case 'msOnBeforeAddToOrder':
        /** @var string $key */
        if ($key == 'mspromocoder_code' && !empty($value)) {
            $check = $msPromocoder->check_code($value);
            if ($check !== true) {
                $modx->event->output($check['message']);
            }
        }
        break;

    case 'msOnCreateOrder':
        /**@var msOrderInterface $order */
        if ($data = $order->get()) {
            /**@var msdCoupon $coupon */
            if (!empty($data['mspromocoder_code']) && $promocode = $modx->getObject('msPromocoderCodes', array('code' => $data['mspromocoder_code']))) {
                /**@var msOrder $msOrder */
                $promocode_arr = $promocode->toArray();
                $promocode_arr['used'] += 1;

                if (!empty($promocode_arr['use_count']) && $promocode_arr['use_count'] <= $promocode_arr['used']) {
                    $promocode_arr['active'] = 0;
                }
                $promocode->fromArray(array(
                    'used' => $promocode_arr['used'],
                    'active' => $promocode_arr['active'],
                ));
                $promocode->save();

                if ($orders = $modx->getObject('msPromocoderOrders', array('code_id' => $promocode_arr['id']))) {
                  $order_ids = json_decode($orders->get('orders'));
                  if (is_array($order_ids)) {
                      $order_ids[] = $msOrder->get('id');
                  }
                  else {
                      $order_ids = array($msOrder->get('id'));
                  }

                  $orders->set('orders', json_encode($order_ids));
                  $orders->save();
                }
                else {
                    // Run processor to save orders attached to promocode.
                    $response = $modx->runProcessor(
                        'mgr/orders/create',
                        array(
                          'code_id' => $promocode_arr['id'],
                          'orders' => json_encode(array($msOrder->get('id')))
                        ),
                        array(
                        'processors_path' => $msPromocoder->config['processorsPath']
                    ));
                }

                $properties = $msOrder->get('properties');
                $code = $promocode->get('code');
                $discount = $promocode->get('discount');

                if (!is_array($properties)) {
                    $properties = array();
                }
                $properties['mspromocoder_code'] = $code;
                $properties['mspromocoder_discount'] = $discount;
                $msOrder->set('properties', $properties);
                $comment = 'Был использован промокод "' . $code . '", скидка ' . $discount;
                $msOrder->set('comment', $comment);
                $msOrder->save();
            }
        }
        break;

}