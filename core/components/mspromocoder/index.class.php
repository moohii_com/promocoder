<?php

/**
 * Class msPromocoderMainController
 */
abstract class msPromocoderMainController extends modExtraManagerController {
	/** @var msPromocoder $msPromocoder */
	public $msPromocoder;


	/**
	 * @return void
	 */
	public function initialize() {
		$version = $this->modx->getVersionData();
		$modx23 = !empty($version) && version_compare($version['full_version'], '2.3.0', '>=');
		// if (!$modx23) {
		// 	$this->addCss($this->msPromocoder->config['cssUrl'] . 'components/msearch2/css/mgr/font-awesome.min.css');
		// }

		$corePath = $this->modx->getOption('mspromocoder_core_path', null, $this->modx->getOption('core_path') . 'components/mspromocoder/');
		require_once $corePath . 'model/mspromocoder/mspromocoder.class.php';

		if (!include_once MODX_CORE_PATH . 'components/minishop2/model/minishop2/minishop2.class.php') {
			throw new Exception('You must install miniShop2 first');
		}

		$this->miniShop2 = new miniShop2($this->modx);
		$this->msPromocoder = new msPromocoder($this->modx);


		//$this->addCss($this->msPromocoder->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->msPromocoder->config['jsUrl'] . 'mgr/mspromocoder.js');
		// miniShop2 js.
		// $this->addJavascript($this->miniShop2->config['jsUrl'] . 'mgr/misc/ms2.utils.js');
		// $this->addJavascript($this->miniShop2->config['jsUrl'] . 'mgr/misc/ms2.combo.js');
		$this->addHtml('
		<script type="text/javascript">
			// miniShop2.config = ' . $this->modx->toJSON($this->miniShop2->config) . ';
			// miniShop2.config.connector_url = "' . $this->miniShop2->config['connectorUrl'] . '";
			msPromocoder.config = ' . $this->modx->toJSON($this->msPromocoder->config) . ';
			msPromocoder.config.connector_url = "' . $this->msPromocoder->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('mspromocoder:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends msPromocoderMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}