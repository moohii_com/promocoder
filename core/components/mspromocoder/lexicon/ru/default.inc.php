<?php
include_once 'setting.inc.php';

$_lang['mspromocoder'] = 'msPromocoder';
$_lang['mspromocoder_menu_title'] = 'Промокоды';
$_lang['mspromocoder_menu_desc'] = 'Управление промокодами';
$_lang['mspromocoder_intro_msg'] = 'Вы можете выделять сразу несколько промокодов при помощи Shift или Ctrl.';

$_lang['mspromocoder_codes'] = 'Промокоды';
$_lang['mspromocoder_codes_common'] = 'Общие';
$_lang['mspromocoder_codes_p'] = 'Для товаров';
$_lang['mspromocoder_codes_cv'] = 'Для категорий/производителей';


// Promocode.
$_lang['mspromocoder_code_id'] = 'Id';
$_lang['mspromocoder_code'] = 'Промокод';
$_lang['mspromocoder_code_type'] = 'Тип промокода';
$_lang['mspromocoder_code_discount'] = 'Скидка';
$_lang['mspromocoder_code_description'] = 'Описание';
$_lang['mspromocoder_code_active'] = 'Активно';
$_lang['mspromocoder_code_begins'] = 'Дата начала';
$_lang['mspromocoder_code_ends'] = 'Дата окончания';
$_lang['mspromocoder_code_active'] = 'Активирован';
$_lang['mspromocoder_code_free_delivery'] = 'Бесплатная доставка';
$_lang['mspromocoder_code_use_count'] = 'Количество';
$_lang['mspromocoder_code_used'] = 'Использовано';
$_lang['mspromocoder_code_min_order_price'] = 'Минимальный заказ';


// Products.
$_lang['mspromocoder_products'] = 'Товары';
$_lang['mspromocoder_products_err_empty'] = 'Выберите один или несколько товаров';

// Product categories.
$_lang['mspromocoder_product_categories'] = 'Категории товаров';

// Product vendors.
$_lang['mspromocoder_vendors'] = 'Прозводители';
$_lang['mspromocoder_vendor'] = 'Прозводитель';

// Orders.
$_lang['mspromocoder_orders'] = 'Заказы';

$_lang['mspromocoder_vc_err_empty'] = 'Заполните одно из полей';


// Actions.
$_lang['mspromocoder_code_create'] = 'Создать промокод';
$_lang['mspromocoder_code_update'] = 'Изменить Промокод';
$_lang['mspromocoder_code_enable'] = 'Включить Промокод';
$_lang['mspromocoder_codes_enable'] = 'Включить Промокоды';
$_lang['mspromocoder_code_disable'] = 'Отключить Промокод';
$_lang['mspromocoder_codes_disable'] = 'Отключить Промокоды';
$_lang['mspromocoder_code_remove'] = 'Удалить Промокод';
$_lang['mspromocoder_codes_remove'] = 'Удалить Промокоды';
$_lang['mspromocoder_code_remove_confirm'] = 'Вы уверены, что хотите удалить этот Промокод?';
$_lang['mspromocoder_codes_remove_confirm'] = 'Вы уверены, что хотите удалить эти Промокоды?';

$_lang['mspromocoder_code_err_name'] = 'Вы должны указать имя Промокода.';
$_lang['mspromocoder_code_err_ae'] = 'Такой промокод уже существует.';
$_lang['mspromocoder_code_err_nf'] = 'Промокод не найден.';
$_lang['mspromocoder_code_err_ns'] = 'Промокод не указан.';
$_lang['mspromocoder_code_err_remove'] = 'Ошибка при удалении Промокода.';
$_lang['mspromocoder_code_err_save'] = 'Ошибка при сохранении Промокода.';

// $_lang['mspromocoder_grid_search'] = 'Поиск';
$_lang['mspromocoder_grid_actions'] = 'Действия';