(function($) {
    // Update cart total price with discount.
    var cart_total_price = $('.ms2_total_cost');
    if (cart_total_price.length > 0) {
        $.post('/assets/components/mspromocoder/action.php', {
            action: 'cart/with_discont',
            only_cart: true,
            promocode: '',
        },
        function(data) {
            refresh_cart(data);
        });
    }

    // Function for replace products price with new.
    function refresh_cart(data) {
        if (data == false) {
            return;
        }
        data = JSON.parse(data);

        $.each(data.products, function(key, item) {
            price = '<span>' + miniShop2.Utils.formatPrice(item['price']) + '</span> руб.';
            if (item['old_price'] != '') {
                price += '<br><span class="old_price">' + miniShop2.Utils.formatPrice(item['old_price']) + ' руб.</span>';
            }
            $('#' + key + ' .price').html(price);
        });

        miniShop2.Cart.status(data['status']);

        return;
    }

    // Minishop callback on promocode add.
    miniShop2.Callbacks.Order.add.ajax.done = function(res) {
        var res = res.responseJSON;
        if (typeof(res.data['mspromocoder_code']) != 'undefined') {
            miniShop2.Order.getcost();
            action = 'cart/with_discont';
            if (res.data['mspromocoder_code'] == '') {
                action = 'cart/without_discont';
            }

            $.post('/assets/components/mspromocoder/action.php', {
                action: action,
                only_cart: true,
                promocode: res.data['mspromocoder_code']
            },
            function(data) {
                refresh_cart(data);
            });
        }
    }

    $('#msPromocoder-check').change(function() {
        var code = $(this).val(),
            promocode_field = $('#msPromocoder-code'),
            coupon_field = $('#coupon_code');

        $.post('/assets/components/mspromocoder/action.php', {
            action: 'check/code',
            promocode: code
        },
        function(check) {
            if (check == 'coupon') {
                coupon_field.val(code).trigger('change');
                promocode_field.val('').trigger('change');
            }
            else {
                promocode_field.val(code).trigger('change');
                if(coupon_field.length > 0) {
                    coupon_field.val('').trigger('change');
                }
            }
        });
    });
})(jQuery);