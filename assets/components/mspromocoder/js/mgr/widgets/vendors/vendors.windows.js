// msPromocoder.window.CreateCode = function (config) {
// 	config = config || {};
// 	if (!config.id) {
// 		config.id = 'mspromocoder-code-window-create';
// 	}
// 	Ext.applyIf(config, {
// 		title: _('mspromocoder_code_create'),
// 		width: 550,
// 		autoHeight: true,
// 		url: msPromocoder.config.connector_url,
// 		action: 'mgr/code/create',
// 		fields: this.getFields(config),
// 		keys: [{
// 			key: Ext.EventObject.ENTER, shift: true, fn: function () {
// 				this.submit()
// 			}, scope: this
// 		}]
// 	});
// 	msPromocoder.window.CreateCode.superclass.constructor.call(this, config);
// };
// Ext.extend(msPromocoder.window.CreateCode, MODx.Window, {

// 	getFields: function (config) {
// 		return [{
//       layout:'column',
//       border: false,
//       anchor: '100%',
//       style: 'margin-bottom: 10px;',
//       items: [{
//         columnWidth: .4,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code'), name: 'code', id: config.id + '-code', anchor: '99%', allowBlank: false}
//         ]
//       }, {
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_discount'), name: 'discount', id: config.id + '-discount', anchor: '99%', allowBlank: false}
//         ]
//       }, {
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_use_count'), name: 'use_count', id: config.id + '-use_count', anchor: '99%'}
//         ]
//       }]
//     }, {
//       layout:'column',
//       border: false,
//       anchor: '100%',
//       style: 'margin-bottom: 10px;',
//       items: [{
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_min_order_price'), name: 'min_order_price', id: config.id + '-min_order_price', anchor: '99%'}
//         ]
//       }, {
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         style: 'padding-top: 10px;',
//         items: [
//           {xtype: 'xcheckbox', boxLabel: _('mspromocoder_code_active'), name: 'active', id: config.id + '-active'}
//         ]
//       }, {
//         columnWidth: .4,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         style: 'padding-top: 10px;',
//         items: [
//           {xtype: 'xcheckbox', boxLabel: _('mspromocoder_code_free_delivery'),	name: 'free_delivery', id: config.id + '-free_delivery'}
//         ]
//       }]
//     }, {
//       layout:'column',
//       border: false,
//       anchor: '100%',      items: [{
//         columnWidth: .5,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'datefield', fieldLabel: _('mspromocoder_code_begins'), name: 'begins', id: config.id + '-begins', anchor: '99%', format: 'd-m-Y'}
//         ]
//       }, {
//         columnWidth: .5,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'datefield', fieldLabel: _('mspromocoder_code_ends'), name: 'ends', id: config.id + '-ends', anchor: '99%', format: 'd-m-Y'}
//         ]
//       }]
//     }, {
// 			xtype: 'textarea',
// 			fieldLabel: _('mspromocoder_code_description'),
// 			name: 'description',
// 			id: config.id + '-description',
// 			height: 50,
// 			anchor: '99%'
// 		}];
// 	},

// 	loadDropZones: function() {
// 	}

// });
// Ext.reg('mspromocoder-code-window-create', msPromocoder.window.CreateCode);


// msPromocoder.window.UpdateCode = function (config) {
// 	config = config || {};
// 	if (!config.id) {
// 		config.id = 'mspromocoder-code-window-update';
// 	}
// 	Ext.applyIf(config, {
// 		title: _('mspromocoder_code_update'),
// 		width: 550,
// 		autoHeight: true,
// 		url: msPromocoder.config.connector_url,
// 		action: 'mgr/code/update',
// 		fields: this.getFields(config),
// 		keys: [{
// 			key: Ext.EventObject.ENTER, shift: true, fn: function () {
// 				this.submit()
// 			}, scope: this
// 		}]
// 	});
// 	msPromocoder.window.UpdateCode.superclass.constructor.call(this, config);
// };
// Ext.extend(msPromocoder.window.UpdateCode, MODx.Window, {

// 	getFields: function (config) {
// 		return [{
//         xtype: 'hidden',
//         name: 'id',
//         id: config.id + '-id'
//       }, {
//       layout:'column',
//       border: false,
//       anchor: '100%',
//       items: [{
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code'), name: 'code', id: config.id + '-code', anchor: '99%', allowBlank: false}
//         ]
//       }, {
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_discount'), name: 'discount', id: config.id + '-discount', anchor: '99%', allowBlank: false}
//         ]
//       }, {
//         columnWidth: .2,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_use_count'), name: 'use_count', id: config.id + '-use_count', anchor: '99%'}
//         ]
//       }, {
//         columnWidth: .2,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_used'), name: 'used', id: config.id + '-used', readOnly: true, anchor: '99%'}
//         ]
//       }]
//     }, {
//       layout:'column',
//       border: false,
//       anchor: '100%',
//       style: 'margin-bottom: 10px;',
//       items: [{
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'textfield', fieldLabel: _('mspromocoder_code_min_order_price'), name: 'min_order_price', id: config.id + '-min_order_price', anchor: '99%'}
//         ]
//       }, {
//         columnWidth: .3,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         style: 'padding-top: 10px;',
//         items: [
//           {xtype: 'xcheckbox', boxLabel: _('mspromocoder_code_active'), name: 'active', id: config.id + '-active'}
//         ]
//       }, {
//         columnWidth: .4,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         style: 'padding-top: 10px;',
//         items: [
//           {xtype: 'xcheckbox', boxLabel: _('mspromocoder_code_free_delivery'),	name: 'free_delivery', id: config.id + '-free_delivery'}
//         ]
//       }]
//     }, {
//       layout:'column',
//       border: false,
//       anchor: '100%',      items: [{
//         columnWidth: .5,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'datefield', fieldLabel: _('mspromocoder_code_begins'), name: 'begins', id: config.id + '-begins', anchor: '99%', format: 'd-m-Y'}
//         ]
//       }, {
//         columnWidth: .5,
//         layout: 'form',
//         defaults: { msgTarget: 'under' },
//         border:false,
//         items: [
//           {xtype: 'datefield', fieldLabel: _('mspromocoder_code_ends'), name: 'ends', id: config.id + '-ends', anchor: '99%', format: 'd-m-Y'}
//         ]
//       }]
//     }, {
// 			xtype: 'textarea',
// 			fieldLabel: _('mspromocoder_code_description'),
// 			name: 'description',
// 			id: config.id + '-description',
// 			height: 50,
// 			anchor: '99%'
// 		}];
// 	},

// 	loadDropZones: function() {
// 	}

// });
// Ext.reg('mspromocoder-code-window-update', msPromocoder.window.UpdateCode);