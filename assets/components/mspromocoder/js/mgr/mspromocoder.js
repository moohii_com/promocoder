var msPromocoder = function (config) {
	config = config || {};
	msPromocoder.superclass.constructor.call(this, config);
};
Ext.extend(msPromocoder, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('mspromocoder', msPromocoder);

msPromocoder = new msPromocoder();