<?php

if (empty($_REQUEST['action']) && empty($_REQUEST['promocode'])) {
  die('Access denied');
}

if (!empty($_REQUEST['action'])) {
  $_REQUEST['ms_promocoder_action'] = $_REQUEST['action'];
}

require dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';